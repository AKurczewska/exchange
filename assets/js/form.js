$(function () {
    $('#money_form').submit(function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var formSerialize = $(this).serialize();

        $.post(url, formSerialize, function(response) {
            var results = '';
            $.each(response, function (key) {
                results += '<div class="row">'
                    + '<div class="col-md-1"><strong>' + key + ':</strong></div>'
                    + '<div class="col-md-11">' + response[key] + '</div>'
                + '</div>';
            });
            $('.results-container').show();
            $('#results').html(results);
        }, 'JSON').fail(function(error) {
            alert(error.responseJSON.message);
        });
    });
});
