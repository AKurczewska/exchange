<?php


namespace App\Service;


use App\Util\NbpExchange;

class MoneyExchangeService
{
    private $nbpExchange;

    public function __construct(NbpExchange $nbpExchange)
    {
        $this->nbpExchange = $nbpExchange;
    }

    public function calculateMoney(array $data)
    {
        $response['gross'] = $this->netToGross($data['amount']);
        $response['eur'] = $this->currencyChange($response['gross'], 'eur');
        $response['usd'] = $this->currencyChange($response['gross'], 'usd');

        return $response;
    }

    private function netToGross(float $net)
    {
        return round($net * 1.23, 2);
    }

    private function currencyChange(float $amount, string $currencyName)
    {
        $currencyValue = $this->nbpExchange->exchangeMoney($currencyName);
        return $currencyValue ? round($amount * $currencyValue, 2) : null;
    }
}