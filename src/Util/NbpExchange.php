<?php


namespace App\Util;


use Symfony\Component\HttpClient\HttpClient;

class NbpExchange
{
    public function exchangeMoney($currencyName)
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://api.nbp.pl/api/exchangerates/rates/a/' . $currencyName);

        if ($response->getStatusCode() != 200) {
            return null;
        }

        $content = $response->toArray();

        return $content['rates'][0]['mid'] ?? null;
    }
}