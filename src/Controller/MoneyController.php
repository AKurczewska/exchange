<?php


namespace App\Controller;


use App\Form\MoneyExchangeFormType;
use App\Service\MoneyExchangeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MoneyController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $form = $this->createForm(MoneyExchangeFormType::class);

        return $this->render('exchange/index.html.twig', [
            'money_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/exchange", name="exchange", methods={"POST"})
     */
    public function exchange(Request $request, MoneyExchangeService $moneyExchangeService)
    {
        $form = $this->createForm(MoneyExchangeFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $exchange = $moneyExchangeService->calculateMoney($data);

            return new JsonResponse($exchange, 200);
        } else {
            return new JsonResponse(['message' => 'Invalid value'], 400);
        }
    }
}